﻿using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;
using Domain.DTO_s;
using Domain.Shared;

namespace Domain.CustomAttribute;

[AttributeUsage( AttributeTargets.Class)]
public sealed class CommunicationDtoValidationAttribute : ValidationAttribute
{
    private const string PatternPhone = "^\\+?[1-9][0-9]{7,14}$";
    private const string PatternEmail = "^\\S+@\\S+\\.\\S+$";

    public override bool IsValid(object? value)
    {
        if (value is not CommunicationDto communicationDto)
            return false;

        switch (communicationDto.Type)
        {
            case CommunicationType.Email when Regex.IsMatch(communicationDto.Value, PatternEmail):
            case CommunicationType.Phone when Regex.IsMatch(communicationDto.Value, PatternPhone):
                return true;
            default:
                return false;
        }
    }
}