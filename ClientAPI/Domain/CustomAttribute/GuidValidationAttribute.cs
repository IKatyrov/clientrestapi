using System.ComponentModel.DataAnnotations;

namespace Domain.CustomAttribute;

[AttributeUsage( AttributeTargets.Property | AttributeTargets.Parameter)]
public sealed class GuidValidationAttribute : ValidationAttribute
{
    public override bool IsValid(object? value)
    {
        try
        {
            var guidParse = value?.ToString();

            if (guidParse is null)
            {
                return false;
            }
            
            var guid = Guid.Parse(guidParse);

            return true;
        }
        catch
        {
            return false;
        }
    }
}