﻿using System.ComponentModel.DataAnnotations;
using System.Globalization;

namespace Domain.CustomAttribute;

[AttributeUsage(AttributeTargets.Property)]
public sealed class DecimalPrecisionAttribute : ValidationAttribute
{
    private readonly uint _decimalPrecision;

    public DecimalPrecisionAttribute(uint decimalPrecision)
    {
        _decimalPrecision = decimalPrecision;
    }

    public override bool IsValid(object? value)
    {
        return value is null || (value is decimal d && HasPrecision(d, _decimalPrecision));
    }

    private static bool HasPrecision(decimal value, uint precision)
    {
        var valueStr = value.ToString(CultureInfo.InvariantCulture);
        var indexOfDot = valueStr.IndexOf('.');
        if (indexOfDot == -1)
        {
            return true;
        }

        return valueStr.Length - indexOfDot - 1 <= precision;
    }
}