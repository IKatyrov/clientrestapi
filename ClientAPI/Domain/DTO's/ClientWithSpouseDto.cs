﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.CustomAttribute;
using Domain.Shared;

namespace Domain.DTO_s;

public class ClientWithSpouseDto
{
    [DefaultValue("Имя")] 
    public string? Name { get; set; }
    [DefaultValue("Фамилия")]
    public string? Surname { get; set; }
    [DefaultValue("Отчество")] 
    public string? Patronymic { get; set; }
    [DataType(DataType.Date)]
    public DateTime Dob { get; set; }
    public ICollection<ChildDto> Children { get; set; } = null!;
    public ICollection<Guid> DocumentIds { get; set; } = null!;
    public PassportDto? Passport { get; set; }
    public AddressDto? LivingAddress { get; set; }
    public AddressDto? RegAddress { get; set; }
    public ICollection<JobDto> Jobs { get; set; } = null!;
    public int? CurWorkExp { get; set; }
    [EnumDataType(typeof(TypeEducation))]
    public TypeEducation TypeEducation { get; set; }
    [DecimalPrecision(2), DefaultValue(55.55)]
    public decimal? MonIncome { get; set; }
    [DecimalPrecision(2), DefaultValue(55.55)]
    public decimal? MonExpenses { get; set; }
    public ICollection<CommunicationDto> Communications { get; set; } = null!;
    public ClientDto? Spouse { get; set; }
}