using System.ComponentModel;
using Domain.CustomAttribute;

namespace Domain.DTO_s;

public class PatchClientDto
{
    [GuidValidation]
    public string ClientID { get; init; } = null!;
    [DefaultValue("Новое имя")]
    public string? Name { get; set; }
    [DefaultValue("Новая фамилия")]
    public string? Surname { get; set; }
    [DefaultValue("Новое отчество")] 
    public string? Patronymic { get; set; }

}