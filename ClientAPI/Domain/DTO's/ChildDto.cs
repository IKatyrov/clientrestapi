﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain.DTO_s;

public class ChildDto
{
    [DefaultValue("Имя")] 
    public string? Name { get; set; }
    [DefaultValue("Фамилия")] 
    public string? Surname { get; set; }
    [DefaultValue("Отчество")]
    public string? Patronymic { get; set; }
    [DataType(DataType.Date)]
    public DateTime Dob { get; set; }
}