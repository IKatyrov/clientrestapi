﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.CustomAttribute;
using Domain.Shared;

namespace Domain.DTO_s;

public class JobDto
{
    [EnumDataType(typeof(TypeJob)), DefaultValue(TypeJob.Main)]
    public TypeJob? Type { get; set; }
    [DataType(DataType.Date)] 
    public DateTime? DateEmp { get; set; }
    [DataType(DataType.Date)] 
    public DateTime? DateDismissal { get; set; }
    [DecimalPrecision(2), DefaultValue(55.55)]
    public decimal? MonIncome { get; set; }
    [DefaultValue(1657182750)]
    public int? Tin { get; set; }
    public AddressDto? FactAddress { get; set; }
    public AddressDto? JurAddress { get; set; }
    [DataType(DataType.PhoneNumber), DefaultValue("79583684385")]
    public string? PhoneNumber { get; set; }
}