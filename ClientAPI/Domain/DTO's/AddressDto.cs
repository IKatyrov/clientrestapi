using System.ComponentModel;

namespace Domain.DTO_s;

public class AddressDto
{
    [DefaultValue("444005")]
    public string? ZipCode { get; set; }
    [DefaultValue("Россия")] 
    public string? Country { get; set; }
    [DefaultValue("Московская обл.")]
    public string? Region { get; set; }
    [DefaultValue("Москва")]
    public string? City { get; set; }
    [DefaultValue("улица Пушкина")]
    public string? Street { get; set; }
    [DefaultValue("Дом Колотушкина")]
    public string? House { get; set; }
    [DefaultValue("101а")]
    public string? Apartment { get; set; }
}