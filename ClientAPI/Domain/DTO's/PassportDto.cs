﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Domain.DTO_s;

public class PassportDto
{
    [DefaultValue(7515)]
    public int Series { get; set; }
    [DefaultValue(936378)] 
    public int Number { get; set; }
    [DefaultValue("Выдан каким-то МВД")]
    public string Giver { get; set; } = null!;
    [DataType(DataType.Date)] 
    public DateTime DateIssued { get; set; }
}