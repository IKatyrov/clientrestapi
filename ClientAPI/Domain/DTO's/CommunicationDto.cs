﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.CustomAttribute;
using Domain.Shared;

namespace Domain.DTO_s;

[CommunicationDtoValidation]
public class CommunicationDto
{
    [EnumDataType(typeof(CommunicationType)), DefaultValue(CommunicationType.Email)]
    public CommunicationType Type { get; set; }
    [DefaultValue("example@gmail.com")] 
    public string Value { get; set; } = null!;
}