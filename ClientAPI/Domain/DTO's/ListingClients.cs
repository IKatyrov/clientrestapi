﻿using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using Domain.Shared;

namespace Domain.DTO_s;

public class ListingClients
{
    [DefaultValue("CreatedAt")]
    public string? SortBy { get; set; }

    [EnumDataType(typeof(SortDirection))]
    public SortDirection SortDirection { get; set; }

    [Range(0, int.MaxValue), DefaultValue(10)]
    public int Limit { get; set; }

    [DefaultValue(1)]
    public int Page { get; set; }

    [DefaultValue("Иль")]
    public string? Search { get; set; }
}