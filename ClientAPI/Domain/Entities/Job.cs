using Domain.Shared;

namespace Domain.Entities;

public class Job : AggregateRoot
{
    public TypeJob? Type { get; set; }
    public DateTime? DateEmp { get; set; }
    public DateTime? DateDismissal { get; set; }
    public decimal? MonIncome { get; set; }
    public int? Tin { get; set; }
    public Address? FactAddress { get; set; }
    public Address? JurAddress { get; set; }
    public string? PhoneNumber { get; set; }
}