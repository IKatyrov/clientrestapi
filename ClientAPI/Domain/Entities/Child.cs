namespace Domain.Entities;

public class Child
{
    public Child()
    {
        Id = Guid.NewGuid();
    }
    public Guid Id { get; init; }
    public string? Name { get; set; }
    public string? Surname { get; set; }
    public string? Patronymic { get; set; }
    public DateTime? Dob { get; set; }
}