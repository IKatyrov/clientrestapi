using System.ComponentModel.DataAnnotations;
using Domain.Shared;

namespace Domain.Entities;

public class Communication : AggregateRoot
{
    public CommunicationType Type { get; set; }
    public string Value { get; set; } = null!;
}