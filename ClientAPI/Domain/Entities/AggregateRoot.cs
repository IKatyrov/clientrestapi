namespace Domain.Entities;

public abstract class AggregateRoot
{
    protected AggregateRoot()
    {
        Id = Guid.NewGuid();
        IsDeleted = false;
        CreatedAt = DateTime.Now;
        UpdatedAt = DateTime.Now;
    }

    public Guid Id { get; init; }
    public DateTime CreatedAt { get; init; }
    public DateTime UpdatedAt { get; set; }
    public bool IsDeleted { get; set; }
}