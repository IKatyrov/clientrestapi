﻿using Domain.Shared;

namespace Domain.Entities;

public class Client : AggregateRoot
{
    public string? Name { get; set; }
    public string? Surname { get; set; }
    public string? Patronymic { get; set; }
    public DateTime Dob { get; set; }
    public ICollection<Child> Children { get; set; } = null!;
    public ICollection<Guid> DocumentIds { get; set; } = null!;
    public Passport? Passport { get; set; }
    public Address? LivingAddress { get; set; }
    public Address? RegAddress { get; set; }
    public ICollection<Job> Jobs { get; set; } = null!;
    public int? CurWorkExp { get => CalculateCurrentWorkExperience(); }
    public TypeEducation TypeEducation { get; set; }
    public decimal? MonIncome { get; set; }
    public decimal? MonExpenses { get; set; }
    public ICollection<Communication> Communications { get; set; } = null!;
    public Guid? SpouseId { get; set; }

    public bool Equals(Client? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Equals(Passport, other.Passport);
    }

    public override bool Equals(object? obj)
    {
        if (ReferenceEquals(null, obj)) return false;
        if (ReferenceEquals(this, obj)) return true;
        if (obj.GetType() != this.GetType()) return false;
        return Equals((Client)obj);
    }

    public override int GetHashCode()
    {
        return (Passport != null ? Passport.GetHashCode() : 0);
    }

    private int? CalculateCurrentWorkExperience()
    {
        DateTime now = DateTime.Today;
        DateTime? dateEmploy = null;

        if (Jobs.Count == 0)
        {
            return null;
        }

        dateEmploy = Jobs.Last().DateEmp;

        if (dateEmploy is null)
        {
            return null;
        }

        var experience = now.Year - dateEmploy.Value.Year;
        
        if (dateEmploy.Value > now.AddYears(-experience)) experience--;

        return experience;
    }
}