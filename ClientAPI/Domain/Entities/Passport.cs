using System.ComponentModel.DataAnnotations;

namespace Domain.Entities;

public class Passport : AggregateRoot
{
    public ushort Series { get; set; }
    public uint Number { get; set; }
    public string Giver { get; set; } = null!;
    public DateTime DateIssued { get; set; }
}