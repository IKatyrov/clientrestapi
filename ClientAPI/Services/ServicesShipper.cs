﻿using AutoMapper;
using Logging;
using Repository;
using Repository.DbContexts;
using Services.Contracts;

namespace Services;

public class ServicesShipper : IServicesShipper
{
    private readonly Lazy<IClientService> _clientService;

    public ServicesShipper(ClientApiDbContext clientApiDbContext, IMapper mapper, ILog log)
    {
        var dbContext = clientApiDbContext ?? throw new ArgumentNullException(nameof(clientApiDbContext));
        var logger = log ?? throw new ArgumentNullException(nameof(log));
        var repositoryWrapper = new RepositoryWrapper(dbContext, logger);
        var map = mapper ?? throw new ArgumentNullException(nameof(mapper));
        
        _clientService = new Lazy<IClientService>(() => new ClientService(repositoryWrapper.Client, map, logger));
    }

    public IClientService ClientService 
        => _clientService.Value;
}