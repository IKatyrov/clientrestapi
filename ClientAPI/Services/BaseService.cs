using Domain.Entities;
using RepositoryContracts;

namespace Services;

internal abstract class BaseService<T> where T : AggregateRoot
{
    private readonly IRepositoryBase<T> _repositoryBase;

    protected BaseService(IRepositoryBase<T> repositoryBase)
    {
        _repositoryBase = repositoryBase;
    }

    public async Task<ICollection<T>> GetAllIncludingRemoved()
    {
        return await _repositoryBase.GetAllIncludingRemoved();
    }

    public async Task<ICollection<T>> GetAll()
    {
        return await _repositoryBase.GetAllAsync();
    }

    public async Task<T?> GetByIdAsync(Guid idGuid)
    {
        return await _repositoryBase.GetByIdAsync(idGuid);
    }

    public async Task<bool> IsExistAsync(Guid idGuid)
    {
        return await _repositoryBase.IsExistAsync(idGuid);
    }

    public async Task<bool> CreateAsync(T entity)
    {
        return await _repositoryBase.CreateEntityAsync(entity);
    }

    public async Task<bool> UpdateAsync(T entity)
    {
        return await _repositoryBase.UpdateAsync(entity);
    }

    public async Task<bool> DeleteAsync(Guid idGuid)
    {
        return await _repositoryBase.DeleteAsync(idGuid);
    }
    
    public async Task<bool> SoftDeleteAsync(Guid idGuid)
    {
        return await _repositoryBase.SoftDeleteAsync(idGuid);
    }
}