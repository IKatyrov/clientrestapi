﻿using System.Linq.Dynamic.Core;
using Domain;
using Domain.Entities;

namespace Services;

public static class QueryableExtension
{
    public static IQueryable<T> SearchByClientTextField<T>(this IQueryable<T> queryable, string? searchBy) where T : Client
    {
        if (string.IsNullOrEmpty(searchBy))
        {
            return queryable;
        }

        var listToReturn = new List<T>(queryable.Count());
        
        foreach (var client in queryable)
        {
            if (CheckContainsField(client,searchBy))
            {
                listToReturn.Add(client);
            }
        }

        return listToReturn.AsQueryable();
    }

    private static bool CheckContainsField(Client client, string searchBy)
    {
        //догадался либо так, либо через рефлексию,
        //путём фич из System.Linq.Dynamic.Core не смог
        
        return !string.IsNullOrEmpty(client.Name) && client.Name.Contains(searchBy) ||
               !string.IsNullOrEmpty(client.Surname) && client.Surname.Contains(searchBy) ||
               !string.IsNullOrEmpty(client.Patronymic) && client.Patronymic.Contains(searchBy);
    }

    public static IQueryable<T> Sort<T>(this IQueryable<T> queryable, string? sortBy, bool ascending) where  T : class
    {
        return string.IsNullOrEmpty(sortBy) ? queryable : queryable.OrderBy(sortBy + (ascending ? " ascending" : " descending"));
    }

    public static PagedModel<T> Paginate<T>(this IQueryable<T> queryable, int? page, int? limit) where T : class
    {
        var paged = new PagedModel<T>();

        if (page.HasValue == false || limit.HasValue == false)
        {
            paged.Items = queryable.ToList();
            paged.CurrentPage = 0;
            paged.PageSize = 0;
            paged.TotalItems = 0;
            paged.TotalPages = 0;
            return paged;
        }
        
        page = (page < 0) ? 1 : page;

        paged.CurrentPage = page.Value;
        paged.PageSize = limit.Value;

        var totalItemsCount =  queryable.Count();

        var startRow = (page - 1) * limit.Value;
        
        paged.Items = queryable
            .Skip(startRow.Value)
            .Take(limit.Value)
            .ToList();

        paged.TotalItems =  totalItemsCount;
        paged.TotalPages = (int)Math.Ceiling(paged.TotalItems / (double)limit);

        return paged;
    }
}