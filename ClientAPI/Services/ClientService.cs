﻿using AutoMapper;
using Domain;
using Domain.DTO_s;
using Domain.Entities;
using Domain.Shared;
using Logging;
using RepositoryContracts;
using Services.Constants;
using Services.Contracts;

namespace Services;

internal class ClientService : BaseService<Client>, IClientService
{
    private readonly IRepositoryBase<Client> _repositoryBase;
    private readonly IMapper _mapper;
    private readonly ILog _log;

    internal ClientService(IRepositoryBase<Client> repositoryBase, IMapper mapper, ILog log) : base(repositoryBase)
    {
        _repositoryBase = repositoryBase;
        _mapper = mapper;
        _log = log;
    }

    public async Task<ServiceResponse<PagedModel<ClientDto>>> GetAllAsync(ListingClients listingClients)
    {
        ServiceResponse<PagedModel<ClientDto>> response = new();

        try
        {
            var clientsList = await _repositoryBase.GetAllAsync();
                
            var pagedModelClient = clientsList
                .AsQueryable()
                .SearchByClientTextField(listingClients.Search)
                .Sort(listingClients.SortBy, listingClients.SortDirection == SortDirection.Asc)
                .Paginate(listingClients.Page, listingClients.Limit);

            if (pagedModelClient.TotalItems == 0)
            {
                response.Message = MessageResponse.NotFound;
                response.Details = ServiceConstants.NotFound;
                return response;
            }

            var pagedModelClientDto = _mapper.Map<PagedModel<ClientDto>>(pagedModelClient);
                
            response.Message = MessageResponse.Ok;
            response.Data = pagedModelClientDto;
        }
        catch (Exception ex)
        {
            _log.LogError(ex.Message);
            response.Message = MessageResponse.Error;
            response.Details = ex.InnerException.Message;
        }

        return response;
    }

    public async Task<ServiceResponse<Guid>> CreateClientAsync(ClientWithSpouseDto clientWithSpouseDto)
    {
        ServiceResponse<Guid> response = new();

        try
        {
            var allClients = await _repositoryBase.GetAllAsync();

            var checkExistPassport = CheckExistPassport(clientWithSpouseDto, allClients);

            if (checkExistPassport is not null)
            {
                response.Message = MessageResponse.Exist;
                response.Details = ServiceConstants.ClientExist + $" серия: {checkExistPassport.Series} номер: {checkExistPassport.Number}";
                return response;
            }

            var client = _mapper.Map<Client>(clientWithSpouseDto);
            var spouse = _mapper.Map<Client>(clientWithSpouseDto.Spouse);

            if (spouse is not null)
            {
                client.SpouseId = spouse.Id;
                spouse.SpouseId = client.Id;
            }

            await AddClient(client, response);
            await AddClient(spouse, response);

            if (response.Message == MessageResponse.RepositoryError)
            {
                return response;
            }
                
            response.Data = client.Id;
            response.Message = MessageResponse.Ok;
            response.Details = ServiceConstants.SuccessAdd;
        }
        catch (Exception ex)
        { 
            _log.LogError(ex.Message);
            response.Message = MessageResponse.Error;
            response.Details = ex.InnerException.Message;
        }

        return response;
    }

    public new async Task<ServiceResponse<ClientWithSpouseDto>> GetByIdAsync(Guid clientId)
    {
        ServiceResponse<ClientWithSpouseDto> response = new();

        try
        {
            var getClient = await _repositoryBase.GetByIdAsync(clientId);

            if (getClient is null)
            {
                response.Message = MessageResponse.NotFound;
                response.Details = ServiceConstants.NotFound;
                return response;
            }

            var clientWithSpouseDtoToReturn = _mapper.Map<ClientWithSpouseDto>(getClient);

            if (getClient.SpouseId.HasValue)
            {
                var existSpouse = await _repositoryBase.IsExistAsync(getClient.SpouseId.Value);

                if (existSpouse)
                {
                    var spouse = await _repositoryBase.GetByIdAsync(getClient.SpouseId.Value);
                    clientWithSpouseDtoToReturn.Spouse = _mapper.Map<ClientDto>(spouse);
                }
            }
                
            response.Message = MessageResponse.Ok;
            response.Data = clientWithSpouseDtoToReturn;
        }
        catch (Exception ex)
        {
            response.Message = MessageResponse.Error;
            response.Details = ex.InnerException?.Message;
            _log.LogError(ex.Message);
        }

        return response;
    }

    public async Task<ServiceResponse<Guid>> UpdateClientAsync(Guid id, PatchClientDto patchClientDto)
    {
        ServiceResponse<Guid> response = new();

        try
        {
            var existingClient = await _repositoryBase.GetByIdAsync(id);

            if (existingClient is null)
            {
                response.Data = id;
                response.Message = MessageResponse.NotFound;
                response.Details = ServiceConstants.NotFound;
                return response;
            }
                
            existingClient.Name = patchClientDto.Name ?? existingClient.Name;
            existingClient.Surname = patchClientDto.Surname ?? existingClient.Surname;
            existingClient.Patronymic = patchClientDto.Patronymic ?? existingClient.Patronymic;
            existingClient.UpdatedAt = DateTime.Now;

            if (!await _repositoryBase.UpdateAsync(existingClient))
            {
                response.Data = id;
                response.Message = MessageResponse.RepositoryError;
                response.Details = ServiceConstants.ErrorRepoUpdate;
                return response;
            }
                
            response.Data = id;
            response.Message = MessageResponse.Ok;
            response.Details = ServiceConstants.SuccessUpdate;

        }
        catch (Exception ex)
        {
            response.Data = id;
            response.Message = MessageResponse.Error;
            response.Details = ex.InnerException?.Message;
            _log.LogError(ex.Message);
        }
        return response;
    }

    public async Task<ServiceResponse<Guid>> SoftDeleteClientAsync(Guid clientId)
    {
        ServiceResponse<Guid> response = new();

        try
        {
            if (!await _repositoryBase.SoftDeleteAsync(clientId))
            {
                response.Data = clientId;
                response.Message = MessageResponse.NotFound;
                response.Details = ServiceConstants.NotFound;
                return response;
            }
                
            response.Data = clientId;
            response.Message = MessageResponse.Ok;
            response.Details = ServiceConstants.SuccessSoftDelete;
        }
        catch (Exception ex)
        {
            response.Data = clientId;
            response.Message = MessageResponse.Error;
            response.Details = ex.InnerException?.Message;
           _log.LogError(ex.Message);
        }
        return response;
    }

    private Passport? CheckExistPassport(ClientWithSpouseDto clientWithSpouseDto, IEnumerable<Client> clients)
    {
        var client = _mapper.Map<Client>(clientWithSpouseDto);
        var spouse = _mapper.Map<Client>(clientWithSpouseDto.Spouse);

        foreach (var clientDb in clients)
        {
            if (clientDb.Equals(client))
            {
                return client.Passport;
            }
            if (spouse != null && clientDb.Equals(spouse))
            {
                return spouse.Passport;
            }
        }
            
        return null;
    }
    
    private async Task AddClient(Client? client, ServiceResponse<Guid> response)
    {
        if (client is null)
        {
            return;
        }

        if (await _repositoryBase.CreateEntityAsync(client))
        {
            return;
        }

        response.Message = MessageResponse.RepositoryError;
        response.Details = ServiceConstants.ErrorRepoAdd;
    }
    
}