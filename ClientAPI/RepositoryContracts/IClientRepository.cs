namespace RepositoryContracts;

public interface IClientRepository : IRepositoryBase<Domain.Entities.Client>
{
}