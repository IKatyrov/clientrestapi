namespace RepositoryContracts;

public interface IRepositoryWrapper
{
     IClientRepository Client { get; }
}