using Domain.Entities;

namespace RepositoryContracts;

public interface IRepositoryBase<T> where T : AggregateRoot
{
    Task<ICollection<T>> GetAllIncludingRemoved();
    Task<ICollection<T>> GetAllAsync();
    Task<T?> GetByIdAsync(Guid idGuid);
    Task<bool> IsExistAsync(Guid idGuid);
    Task<bool> CreateEntityAsync(T entity);
    Task<bool> UpdateAsync(T entity);
    Task<bool> DeleteAsync(Guid idGuid);
    Task<bool> SoftDeleteAsync(Guid idGuid);
    Task<bool> SaveAsync();
}