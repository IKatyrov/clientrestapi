namespace Services.Contracts;

public interface IServicesShipper
{
    public IClientService ClientService { get; }
}