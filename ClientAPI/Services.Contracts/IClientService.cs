using Domain;
using Domain.DTO_s;

namespace Services.Contracts;

public interface IClientService
{
    Task<ServiceResponse<PagedModel<ClientDto>>> GetAllAsync(ListingClients listingClients);
    Task<ServiceResponse<Guid>>  CreateClientAsync(ClientWithSpouseDto clientWithSpouseDto);
    Task<ServiceResponse<ClientWithSpouseDto>> GetByIdAsync(Guid clientId);
    Task<ServiceResponse<Guid>>  UpdateClientAsync(Guid id, PatchClientDto patchClientDto);
    Task<ServiceResponse<Guid>> SoftDeleteClientAsync(Guid clientId);
}

