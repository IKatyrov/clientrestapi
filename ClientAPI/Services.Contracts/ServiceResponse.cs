﻿using Services.Constants;

namespace Services.Contracts;

public class ServiceResponse<T>
{
    public T? Data { get; set; }
    public MessageResponse Message { get; set; } = MessageResponse.None;
    public string? Details { get; set; }
}