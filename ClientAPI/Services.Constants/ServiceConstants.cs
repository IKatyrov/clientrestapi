namespace Services.Constants;

public static class ServiceConstants
{
    public const string NotFound = "Данные не найдены";
    public const string ErrorRepoAdd = "Ошибка на уровне репозитория при добавлении записи";
    public const string ErrorRepoUpdate = "Ошибка на уровне репозитория при обновлении записи";       
    public const string SuccessAdd = "Данные успешно добавлены";
    public const string SuccessUpdate = "Данные успешно обновлены";
    public const string SuccessSoftDelete = "Данные успешно мягко удалены";
    public const string ClientExist = "Клиент с паспортом существует";
}