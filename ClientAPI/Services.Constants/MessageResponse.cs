﻿namespace Services.Constants;

public enum MessageResponse
{
    None,
    Exist,
    RepositoryError,
    Error,
    NotFound,
    Ok,
}