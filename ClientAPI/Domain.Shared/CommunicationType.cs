﻿namespace Domain.Shared;

public enum CommunicationType
{
    Email,
    Phone
}