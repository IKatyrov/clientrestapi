﻿namespace Domain.Shared;

public enum SortDirection
{
    Asc,
    Desc
}