﻿namespace Domain.Shared;

public enum TypeEducation
{
    Secondary, 
    SecondarySpecial, 
    IncompleteHigher, 
    Higher,
    TwoOrMoreHigher,
    AcademicDegree,
}