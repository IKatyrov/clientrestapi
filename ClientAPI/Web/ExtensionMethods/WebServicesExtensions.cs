using Logging;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json.Converters;
using NLog;
using Repository.DbContexts;
using Services;
using Services.Contracts;

namespace Web.ExtensionMethods;

public static class WebServicesExtensions
{
    public static void ConfigureSqlContext(this IServiceCollection services, IConfiguration config)
    {
        services.AddDbContext<ClientApiDbContext>(contextOptionsBuilder =>
        {
            contextOptionsBuilder.UseSqlServer(config.GetConnectionString("DbConnectionStr"), sqlServerOptions =>
            {
                sqlServerOptions.EnableRetryOnFailure(
                    maxRetryCount: 3,
                    maxRetryDelay: TimeSpan.FromSeconds(3),
                    errorNumbersToAdd: new List<int> { 4060 });
            });
        });
    }

    public static void ConfigureServiceShipper(this IServiceCollection services)
    {
        services.AddScoped<IServicesShipper, ServicesShipper>();
    }

    public static void ConfigureNewtonsoftJsonService(this IServiceCollection services)
    {
        services.AddControllersWithViews().AddNewtonsoftJson(opt =>
        {
            opt.SerializerSettings.Converters.Add(new StringEnumConverter());
        });
        services.AddSwaggerGenNewtonsoftSupport();
    }

    public static void ConfigureLoggerService(this IServiceCollection services)
    {
        LogManager.LoadConfiguration(string.Concat(Directory.GetCurrentDirectory(), "/nlog.config"));
        services.AddSingleton<ILog, Log>();
    }
}