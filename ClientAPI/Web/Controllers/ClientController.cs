using Domain;
using Domain.CustomAttribute;
using Domain.DTO_s;
using Logging;
using Microsoft.AspNetCore.Mvc;
using Services.Constants;
using Services.Contracts;
using Web.ExtensionMethods;

namespace Web.Controllers;

[ApiController]
[Route("[controller]")]
public class ClientController : ControllerBase
{
    private readonly IClientService _clientService;
    private readonly ILog _log;

    public ClientController(IServicesShipper servicesShipper, ILog log)
    {
        var shipper = servicesShipper ?? throw new ArgumentNullException(nameof(servicesShipper));
        _log = log ?? throw new ArgumentNullException(nameof(log));

        _clientService = servicesShipper.ClientService;
    }

    [HttpGet]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(PagedModel<ClientDto>))]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
    public async Task<IActionResult> GetAll([FromQuery] ListingClients listingClients)
    {
        _log.LogInfo("Поступил запрос на всех клиентов");

        var clients = await _clientService.GetAllAsync(listingClients);

        return clients.Message switch
        {
            MessageResponse.Ok => Ok(clients.Data),
            MessageResponse.NotFound => NotFound(clients.Details),
            _ => StatusCode(500, clients.Details)
        };
    }

    [HttpPost(Name = "Guid")]
    [ProducesResponseType(StatusCodes.Status201Created)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity, Type = typeof(List<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
    public async Task<IActionResult> CreateClient([FromBody] ClientWithSpouseDto clientWithSpouseDto)
    {
        _log.LogInfo("Поступил запрос на создание клиента");

        //здесь я не смог вернуть 422 ошибку по валидации,
        //даже если вернуть UnprocessableEntityObjectResult, то всё равно идёт 400 ошибка
        if (!ModelState.IsValid)
        {
            _log.LogError("Модель не прошла валидацию");
            return StatusCode(422, ModelState.GetErrorMessages());
        }

        var newClientDto = await _clientService.CreateClientAsync(clientWithSpouseDto);
        ;

        return newClientDto.Message switch
        {
            MessageResponse.Exist => StatusCode(422, newClientDto.Details),
            MessageResponse.RepositoryError => StatusCode(422, newClientDto.Details),
            MessageResponse.Ok => CreatedAtRoute("Guid", new { Guid = newClientDto.Data }, newClientDto.Data),
            _ => StatusCode(500, newClientDto.Details)
        };
    }

    [HttpGet("{ClientID}")]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(ClientWithSpouseDto))]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity, Type = typeof(List<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
    [ProducesDefaultResponseType]
    public async Task<IActionResult> GetById([GuidValidation] string ClientID)
    {
        _log.LogInfo("Поступил запрос на получение клиента");

        if (!ModelState.IsValid)
        {
            _log.LogError("Guid не прошёл валидацию");
            return StatusCode(422, ModelState.GetErrorMessages());
        }

        Guid id = Guid.Parse(ClientID);

        var client = await _clientService.GetByIdAsync(id);

        return client.Message switch
        {
            MessageResponse.NotFound => NotFound(client.Details),
            MessageResponse.Ok => Ok(client.Data),
            _ => StatusCode(500, client.Details)
        };
    }

    [HttpPatch(nameof(PatchClientDto.ClientID))]
    [ProducesResponseType(StatusCodes.Status200OK, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status404NotFound, Type = typeof(string))]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity, Type = typeof(List<string>))]
    [ProducesResponseType(StatusCodes.Status500InternalServerError, Type = typeof(string))]
    public async Task<IActionResult> UpdateClient([FromQuery] PatchClientDto patchClientDto)
    {
        _log.LogInfo("Поступил запрос на обновление клиента");

        //мне было уже лень писать обновления для сложных типов,
        //оставлю пока примитивы и строки, если нужно, то допишу

        if (!ModelState.IsValid)
        {
            _log.LogError("Модель не прошла валидацию");
            return StatusCode(422, ModelState.GetErrorMessages());
        }

        Guid id = Guid.Parse(patchClientDto.ClientID);

        var updateClient = await _clientService.UpdateClientAsync(id, patchClientDto);

        return updateClient.Message switch
        {
            MessageResponse.Ok => Ok(updateClient.Details),
            MessageResponse.NotFound => NotFound(updateClient.Details),
            MessageResponse.RepositoryError => StatusCode(422, updateClient.Details),
            _ => StatusCode(500, updateClient.Details)
        };
    }

    [HttpDelete("{ClientID}")]
    [ProducesResponseType(StatusCodes.Status200OK)]
    [ProducesResponseType(StatusCodes.Status404NotFound)]
    [ProducesResponseType(StatusCodes.Status422UnprocessableEntity)]
    [ProducesResponseType(StatusCodes.Status500InternalServerError)]
    public async Task<IActionResult> DeleteClient([GuidValidation] string ClientID)
    {
        if (!ModelState.IsValid)
        {
            _log.LogError("Модель не прошла валидацию");
            return StatusCode(422, ModelState.GetErrorMessages());
        }

        Guid id = Guid.Parse(ClientID);

        _log.LogInfo("Поступил запрос на мягкое удаление клиента");
        var deleteClient = await _clientService.SoftDeleteClientAsync(id);

        return deleteClient.Message switch
        {
            MessageResponse.Ok => Ok(deleteClient.Details),
            MessageResponse.NotFound => NotFound(deleteClient.Details),
            MessageResponse.RepositoryError => StatusCode(422, deleteClient.Details),
            _ => StatusCode(500, deleteClient.Details)
        };
    }

}