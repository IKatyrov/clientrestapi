﻿using AutoMapper;
using Domain.DTO_s;
using Domain.Entities;

namespace Web.Mapping;

public class DtoToModelProfile : Profile
{
    public DtoToModelProfile()
    {
        CreateMap<ClientWithSpouseDto, Client>();
        CreateMap<ClientDto, Client>();
        CreateMap<ChildDto, Child>();
        CreateMap<PassportDto, Passport>();
        CreateMap<AddressDto, Address>();
        CreateMap<JobDto, Job>();
        CreateMap<CommunicationDto, Communication>();
    }
}