using AutoMapper;
using Domain;
using Domain.DTO_s;
using Domain.Entities;

namespace Web.Mapping;

public class ModelToDtoProfile : Profile
{
    public ModelToDtoProfile()
    {
        CreateMap<PagedModel<Client>, PagedModel<ClientDto>>();
        CreateMap<Client, ClientDto>();
        CreateMap<Client, ClientWithSpouseDto>();
        CreateMap<Child, ChildDto>();
        CreateMap<Passport, PassportDto>();
        CreateMap<Address, AddressDto>();
        CreateMap<Job, JobDto>();
        CreateMap<Communication, CommunicationDto>();
    }
}