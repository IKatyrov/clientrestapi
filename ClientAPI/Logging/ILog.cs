﻿namespace Logging;

public interface ILog
{
    void LogInfo(string message);
    void LogError(string message);
}