﻿using NLog;

namespace Logging;

public class Log : ILog
{
    private static ILogger log = LogManager.GetCurrentClassLogger();

    public void LogError(string message) => log.Error(message);
    public void LogInfo(string message) => log.Info(message);
}