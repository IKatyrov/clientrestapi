﻿using Logging;
using Repository.Client;
using Repository.DbContexts;
using RepositoryContracts;

namespace Repository;

public class RepositoryWrapper : IRepositoryWrapper
{
    private readonly Lazy<IClientRepository> _clientRepository;

    public RepositoryWrapper(ClientApiDbContext clientApiDbContext, ILog log)
    {
        var dbContext = clientApiDbContext ?? throw new ArgumentNullException(nameof(clientApiDbContext));

        _clientRepository = new Lazy<IClientRepository>(()=> new ClientRepository(dbContext, log));
    }
    
    public IClientRepository Client
        => _clientRepository.Value;
}