﻿using Domain.Entities;
using Logging;
using Microsoft.EntityFrameworkCore;
using Repository.DbContexts;
using RepositoryContracts;

namespace Repository;

internal class RepositoryBase<T> : IRepositoryBase<T> where T : AggregateRoot
{
    protected readonly ILog _log;
    protected readonly ClientApiDbContext _dbContext;
    protected readonly DbSet<T> _dbSet;

    internal RepositoryBase(ClientApiDbContext dbContext, ILog log)
    {
        _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        _log = log ?? throw new ArgumentNullException(nameof(log)); ;
        _dbSet = _dbContext.Set<T>();
    }

    public async Task<ICollection<T>> GetAllIncludingRemoved()
    {
       _log.LogInfo("Запрос к БД на получение всех записей");
        return await _dbSet.ToListAsync();
    }

    public async Task<ICollection<T>> GetAllAsync()
    {
        _log.LogInfo("Запрос к БД на получение всех записей не отмеченных как удалённые");
        return await _dbSet.Where(entity => entity.IsDeleted == false).ToListAsync();
    }

    public async Task<T?> GetByIdAsync(Guid idGuid)
    {
        _log.LogInfo("Запрос к БД на получение записи");
        return await _dbSet.FirstOrDefaultAsync(entity => entity.Id.Equals(idGuid));
    }

    public async Task<bool> IsExistAsync(Guid idGuid)
    {
        _log.LogInfo("Запрос к БД на проверку на наличие");
        return await _dbSet.AnyAsync(entity => entity.Id.Equals(idGuid));
    }

    public async Task<bool> CreateEntityAsync(T entity)
    {
       _log.LogInfo("Запрос к БД на добавление");
        await _dbSet.AddAsync(entity);
        return await SaveAsync();
    }

    public async Task<bool> UpdateAsync(T entity)
    {
        _log.LogInfo("Запрос к БД на обновление");
        _dbSet.Update(entity);
        return await SaveAsync();
    }

    public async Task<bool> DeleteAsync(Guid idGuid)
    {
        _log.LogInfo("Запрос к БД на удаление записи");

        var entity = await GetByIdAsync(idGuid);

        if (entity is null)
        {
            return false;
        }

        _dbSet.Remove(entity);
        return await SaveAsync();
    }


    public async Task<bool> SoftDeleteAsync(Guid idGuid)
    {
        _log.LogInfo("Запрос к БД на мягкое удаление записи");
        var existingEntity = await GetByIdAsync(idGuid);

        if (existingEntity is null)
            return false;
            
        existingEntity.IsDeleted = true;
        existingEntity.UpdatedAt = DateTime.Now;
        return await SaveAsync();
    }
    
    public async Task<bool> SaveAsync()
    {
        _log.LogInfo($"Запрос к БД сохранение {typeof(T).Name}");
        return await _dbContext.SaveChangesAsync() >= 0;
    }
}