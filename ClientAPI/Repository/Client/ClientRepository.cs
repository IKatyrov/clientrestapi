﻿using Logging;
using Repository.DbContexts;
using RepositoryContracts;

namespace Repository.Client;

internal class ClientRepository : RepositoryBase<Domain.Entities.Client>, IClientRepository
{
    internal ClientRepository(ClientApiDbContext dbContext, ILog log) : base(dbContext, log)
    {
    }
}