﻿using Microsoft.EntityFrameworkCore;

namespace Repository.DbContexts;

public class ClientApiDbContext : DbContext
{
    public ClientApiDbContext(DbContextOptions<ClientApiDbContext> context) : base(context)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
        new ClientEntityTypeConfiguration().Configure(modelBuilder.Entity<Domain.Entities.Client>());

        base.OnModelCreating(modelBuilder);
    }
}