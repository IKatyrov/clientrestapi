﻿using System.ComponentModel.DataAnnotations;
using Domain.Shared;
using Innofactor.EfCoreJsonValueConverter;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;

namespace Repository.DbContexts;

public class ClientEntityTypeConfiguration : AggregateRootTypeConfiguration<Domain.Entities.Client>
{
    public override void Configure(EntityTypeBuilder<Domain.Entities.Client> builder)
    {
        base.Configure(builder);

        builder.Property(client => client.Name).HasMaxLength(50);
        builder.Property(client => client.Surname).HasMaxLength(50);
        builder.Property(client => client.Patronymic).HasMaxLength(50);
        builder.Property(client => client.Dob).HasColumnType(nameof(DataType.Date));
        builder.Property(client => client.Children).HasJsonValueConversion();
        builder.Property(client => client.DocumentIds).HasJsonValueConversion();
        builder.Property(client => client.Passport).HasJsonValueConversion();
        builder.Property(client => client.RegAddress).HasJsonValueConversion();
        builder.Property(client => client.LivingAddress).HasJsonValueConversion();
        builder.Property(client => client.Jobs).HasJsonValueConversion();
        builder.Property(client => client.TypeEducation).HasConversion(new EnumToStringConverter<TypeEducation>());
        builder.Property(x => x.MonIncome).HasPrecision(10, 2);
        builder.Property(x => x.MonExpenses).HasPrecision(10, 2);
        builder.Property(client => client.Communications).HasJsonValueConversion();
    }
}