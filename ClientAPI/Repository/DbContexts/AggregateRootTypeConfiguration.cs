﻿using System.ComponentModel.DataAnnotations;
using Domain.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Repository.DbContexts;

public abstract class AggregateRootTypeConfiguration<TEntity> : IEntityTypeConfiguration<TEntity>
    where TEntity : AggregateRoot
{
    public virtual void Configure(EntityTypeBuilder<TEntity> builder)
    {
        builder.HasKey(x => x.Id);
        builder.Property(x => x.Id)
            .IsRequired();
        
        builder.Property(x => x.CreatedAt)
            .HasColumnType(nameof(DataType.DateTime))
            .IsRequired();

        builder.Property(x => x.UpdatedAt)
            .HasColumnType(nameof(DataType.DateTime))
            .IsRequired();
        
        builder.Property(x => x.IsDeleted)
            .IsRequired();
    }
}